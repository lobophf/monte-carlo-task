#include"samples.h"

Samples::Samples(){

	n = 0;
	mean = 0.0;
	m2 = 0.0;
	delta = 0.0;
	zScore = 3.0;
	s2 = 1.0; // sample variance
}

Samples::~Samples(){

}


double Samples::getMean(){
	return mean;
}

double Samples::getError(){
	return zScore * sqrt(s2/ n);
}

double Samples::getConfidenceCoefficient(){
	boost::math::normal s(0.0, 1.0);
	return cdf(s, zScore) - cdf(s, - zScore);
}

void Samples::addVariable(double x){
	updateValues(x);
}

void Samples::updateValues(double x){
	n++;
	delta = x - mean;	
	mean += delta/float(n);	
	m2 += delta * (x - mean);
	s2 = m2 / (n - 1);
}

void Samples::setZscore(double zScore){
	this -> zScore = zScore;
}

double Samples::getZscore(){
	return zScore;
}
