#ifndef SAMPLES_H
#define SAMPLES_H

#include<boost/math/distributions/normal.hpp> // for normal_distribution

class Samples{
public:
	Samples();
	~Samples();
	double getMean();
	double getError();
	double getZscore();
	double getConfidenceCoefficient();

	void addVariable(double x);
	void setZscore(double zScore);

private:

	int n;
	double mean;
	double m2;
	double delta;
	double zScore;
	double s2; // sample variance

	void updateValues(double x);
	
};
#endif
