all: main samples
	g++ -o main.out main.o samples.o 

samples:
	g++ -c samples.cpp -o samples.o

main: samples.h
	g++ -c main.cpp -o main.o

run:
	./main.out
