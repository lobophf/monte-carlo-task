#include<iostream>
#include<math.h>
#include<stdlib.h>
#include<time.h>
#include<iomanip>
#include<vector>	// std::vector
#include"samples.h"

float randomDouble(float min, float max);
int digitTrunc(double value);
void fillVector(std::vector<double> &v);

int main(){
	
	srand(time(NULL));
	double error;
	double r;
	int n = 1E7;

	Samples s;

	s.setZscore(4.0);

	double deltaX = 0.0;
	double deltaY = 0.0;
	
	std::vector<double> v;

	for(int i = 0; i < n; i++){	
		
		fillVector(v);
		deltaX = v.at(1) - v.at(0);
		deltaY = v.at(3) - v.at(2);
		r = sqrt(pow(deltaX, 2) + pow(deltaY, 2));
		s.addVariable(r);
	}

	error = s.getError();
	std::cout << std::fixed << std::setprecision(digitTrunc(error)) 
		  << "value: " << s.getMean() << " +/- "
		  << error << "; confidence coefficient: " << std::setprecision(4)
		  << s.getConfidenceCoefficient()
		  << std::endl;

	return 0;
}

float randomDouble(float min, float max){
	return (max - min) * ((float)rand() / (float)RAND_MAX) + min;
}

int digitTrunc(double value){
	int n = 0;	
	while(value < 1.0){
		value *= 10;	
		n++;
	}
	return n;
}

void fillVector(std::vector<double> &v){
	v.clear();
	for(int i = 0; i < 4; i++){
		v.push_back(randomDouble(0.0, 1.0));	
	}
}
